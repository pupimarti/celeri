import React, { useRef } from "react";
import Webcam from "react-webcam";
import Button from "../Button";

import "./styles.css";

export default function CaptureImage({
  withoutDniBox = false,
  videoConstraints = {},
  onScreenShot,
}) {
  const webcamRef = useRef();
  const handleCapture = () => {
    const imageSrc = webcamRef.current.getScreenshot();
    onScreenShot(imageSrc);
  };

  return (
    <>
      {!withoutDniBox && <p>Coloca tu DNI dentro del recuadro</p>}
      <div className="container-camera">
        <Webcam
          width="100%"
          audio={false}
          ref={webcamRef}
          screenshotFormat="image/jpeg"
          videoConstraints={videoConstraints}
        />
        {!withoutDniBox && (
          <div className="container-camera-dni-box">
            <div className="camera-dni-box" />
          </div>
        )}
      </div>
      <Button text="Tomar foto" onClick={handleCapture} />
    </>
  );
}
