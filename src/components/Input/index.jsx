import React, { useId } from "react";

import "./styles.css";

export default function Input({
  Icon,
  placeholder = "",
  type = "text",
  value = "",
  onChange = () => {},
  select = false,
  label = "",
  name = "",
  options = [""],
  required = false,
}) {
  const inputId = useId();
  return (
    <div className="container-input">
      <label htmlFor={inputId}>
        {label.length > 0 && <span className="input-label">{label}</span>}
        <div className="container-input-and-icon">
          {select ? (
            <select
              className={`input input-select ${Icon ? "input-with-icon" : ""}`}
              id={inputId}
              name="name"
              placeholder={placeholder}
              value={value}
              required={required}
              style={{ color: value === "" ? "var(--grey)" : "var(--base)" }}
              onChange={(e) => onChange({ value: e.target.value, name })}
            >
              <option className="disabledOption" disabled value="">
                {placeholder}
              </option>
              {options?.map((val, index) => (
                <option value={val} key={`${index}${value}`}>
                  {val}
                </option>
              ))}
            </select>
          ) : (
            <input
              className={`input ${Icon ? "input-with-icon" : ""}`}
              id={inputId}
              name="name"
              required={required}
              value={value}
              type={type}
              placeholder={placeholder}
              onChange={(e) => onChange({ value: e.target.value, name })}
            />
          )}
          {Icon && (
            <div className="input-icon">
              <Icon color="var(--grey)" />
            </div>
          )}
        </div>
      </label>
    </div>
  );
}
