/* eslint-disable jsx-a11y/click-events-have-key-events */
import React from "react";
import { BsCheck } from "react-icons/bs";

import "./styles.css";

export default function Checkbox({ radius = false, checked = false, onClick }) {
  const handleChange = () => {
    if (onClick) onClick();
  };
  return (
    <div
      onClick={handleChange}
      role="button"
      tabIndex={-1}
      className={`container-checkbox ${
        radius ? "container-checkbox-radius" : ""
      } ${checked ? "checkbox-checked" : ""}`}
    >
      {radius ? (
        checked && <div className="checkbox-radius" />
      ) : (
        <BsCheck color="white" size={20} />
      )}
    </div>
  );
}
