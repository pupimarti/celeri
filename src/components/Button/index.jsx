import React from "react";

import "./styles.css";

const buttonTypes = {
  primary: "primary",
  secondary: "secondary",
};

export default function Button({
  type = buttonTypes.primary,
  onClick = () => {},
  text = "",
  disabled = false,
  submit = false,
}) {
  return (
    <button
      className={`button ${
        type === buttonTypes.secondary ? "button-secondary" : ""
      } ${disabled ? "button-disabled" : ""}`}
      disabled={disabled}
      type={submit ? "submit" : "button"}
      onClick={onClick}
    >
      {text}
    </button>
  );
}
