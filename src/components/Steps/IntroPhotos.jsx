import React from "react";
import Button from "../Button";

export default function IntroPhotos({ nextStep }) {
  return (
    <div>
      <p>¡Excelente!</p>
      <p>Ahora necesitamos fotos de tu DNI y una selfie.</p>
      <Button text="Comenzar" onClick={() => nextStep()} />
    </div>
  );
}
