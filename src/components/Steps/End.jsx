import React from "react";
import Button from "../Button";

export default function End({ nextStep }) {
  return (
    <div>
      <p>¡Excelente, ya terminamos!</p>
      <p>Pronto nos pondremos en contacto con vos.</p>
      <Button text="Finalizar" onClick={() => nextStep()} />
    </div>
  );
}
