import React, { useEffect } from "react";
import saveData from "src/services/saveData";
import Spinner from "../Spinner";

export default function SaveData({ nextStep }) {
  useEffect(() => {
    saveData().then(nextStep);
  }, []);

  return (
    <div>
      <p>Estamos guardando la información, por favor aguarde</p>
      <div
        style={{
          display: "flex",
          justifyContent: "center",
          margin: "20px 0",
        }}
      >
        <Spinner />
      </div>
    </div>
  );
}
