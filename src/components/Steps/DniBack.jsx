import React, { useId, useState } from "react";
import { getBase64, handleSaveStepData, steps } from "src/utils";
import Button from "../Button";
import CaptureImage from "../CaptureImage";

const initialData = {
  img: null,
};

export default function DniBack() {
  const [data, setData] = useState(
    JSON.parse(localStorage.getItem(`data-step${steps.DNI_BACK}`)) ||
      initialData
  );
  const [takePhoto, setTakePhoto] = useState(false);

  const fileId = useId();

  const handleChangeImage = async (e) => {
    const imageFile = e.target.files[0];
    getBase64(imageFile).then((base64) => {
      setData({ img: base64 });
    });
  };

  const handleTakePhoto = (image) => {
    setTakePhoto(false);
    setData({ img: image });
  };

  if (takePhoto) return <CaptureImage onScreenShot={handleTakePhoto} />;

  if (data.img)
    return (
      <div>
        <img src={data.img} alt="Imagen frente del DNI" className="dni-image" />
        <div className="container-buttons">
          <Button
            text="Tomar otra foto"
            type="secondary"
            onClick={() => setData({ img: null })}
          />

          <Button
            text="Confirmar"
            submit
            onClick={() => handleSaveStepData({ step: steps.DNI_BACK, data })}
          />
        </div>
      </div>
    );

  return (
    <section className="form-section">
      <p>¡Excelente!</p>
      <p>Ahora continuaremos con una foto del dorso del DNI</p>
      <div style={{ flexDirection: "column" }} className="container-buttons">
        <Button text="Sacar foto" onClick={() => setTakePhoto(true)} />
        <label htmlFor={fileId} className="container-upload-image">
          <span className="button">Subir archivo</span>
          <input
            type="file"
            id={fileId}
            className="invisible"
            accept="image/x-png,image/gif,image/jpeg"
            onChange={handleChangeImage}
          />
        </label>
      </div>
    </section>
  );
}
