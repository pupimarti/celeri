import React, { useEffect, useMemo, useRef, useState } from "react";
import { steps } from "src/utils";
import AdressData from "./AdressData";
import BankData from "./BankData";
import DniBack from "./DniBack";
import DniFront from "./DniFront";
import End from "./End";
import Error from "./Error";
import InitialData from "./InitialData";
import Intro from "./Intro";
import IntroPhotos from "./IntroPhotos";
import LegalData from "./LegalData";
import PersonalData from "./PersonalData";
import SaveData from "./SaveData";
import Selfie from "./Selfie";

import "./styles.css";

const stepComponents = {
  [steps.INTRO]: Intro,
  [steps.INITIAL_DATA]: InitialData,
  [steps.PERSONAL_DATA]: PersonalData,
  [steps.ADDRESS_DATA]: AdressData,
  [steps.LEGAL_DATA]: LegalData,
  [steps.BANK_DATA]: BankData,
  [steps.SAVE_DATA]: SaveData,
  [steps.INTRO_PHOTOS]: IntroPhotos,
  [steps.DNI_FRONT]: DniFront,
  [steps.DNI_BACK]: DniBack,
  [steps.SELFIE]: Selfie,
  [steps.END]: End,
};

const directions = {
  next: "next",
  prev: "prev",
};

export default function Steps({
  step = steps.INTRO,
  resetSteps,
  nextStep,
  prevStep,
}) {
  const Step = useMemo(() => stepComponents[step], [step]);
  const [direction, setDirection] = useState(directions.next);
  const [animationOut, setAnimationOut] = useState(false);

  const formRef = useRef();

  const handleNextStep = (e) => {
    if (e?.preventDefault) e.preventDefault();
    localStorage.setItem("step", step + 1);
    setDirection(directions.next);
    setAnimationOut(true);
  };

  const handlePrevStep = () => {
    setDirection(directions.prev);
    setAnimationOut(true);
  };

  const handleOnEndAnimation = (e) => {
    if (e.animationName === "disappear-left") {
      setAnimationOut(false);
      nextStep();
    }
    if (e.animationName === "disappear-right") {
      setAnimationOut(false);
      prevStep();
    }
  };

  const getAnimation = () => {
    if (direction === directions.next) {
      return animationOut ? "disappear-left" : "appear-right";
    }
    return animationOut ? "disappear-right" : "appear-left";
  };

  useEffect(() => {
    if (formRef.current) {
      formRef.current.scroll(0, 0);
    }
  }, [step, formRef]);

  return (
    <form ref={formRef} onSubmit={handleNextStep} className="container-steps">
      <div
        onAnimationEnd={handleOnEndAnimation}
        className={`container-sections ${getAnimation()}`}
      >
        {Step ? (
          <Step prevStep={handlePrevStep} nextStep={handleNextStep} />
        ) : (
          <Error resetSteps={resetSteps} />
        )}
      </div>
    </form>
  );
}
