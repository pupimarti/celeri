import React, { useState } from "react";
import { handleSaveStepData, Icons, steps } from "src/utils";
import Button from "../Button";
import Input from "../Input";

const initialData = {
  cbu: "",
  typeAccount: "",
  currency: "",
  entity: "",
};

export default function BankData({ prevStep }) {
  const [data, setData] = useState(
    JSON.parse(localStorage.getItem(`data-step${steps.BANK_DATA}`)) ||
      initialData
  );
  const handleChangeData = ({ value, name }) =>
    setData((prevData) => ({ ...prevData, [name]: value }));
  return (
    <section className="form-section">
      <p>Para continuar, agregá una cuenta bancaria:</p>
      <Input
        Icon={Icons.CBU}
        placeholder="CBU/CVU"
        value={data.cbu}
        name="cbu"
        type="number"
        required
        onChange={handleChangeData}
      />
      <Input
        Icon={Icons.CBU}
        placeholder="Tipo de Cuenta"
        value={data.typeAccount}
        name="typeAccount"
        required
        select
        options={["Caja de Ahorro", "Cuenta Corriente", "Otro"]}
        onChange={handleChangeData}
      />
      <Input
        Icon={Icons.MONEY}
        placeholder="Moneda"
        value={data.currency}
        name="currency"
        required
        select
        options={["ARS", "USD", "Bimonetaria"]}
        onChange={handleChangeData}
      />
      <Input
        Icon={Icons.entity}
        label="Entidad"
        value={data.entity}
        required
        name="entity"
        onChange={handleChangeData}
      />
      <div className="container-buttons">
        <Button text="Anterior paso" type="secondary" onClick={prevStep} />
        <Button
          text="Próximo paso"
          submit
          onClick={() => handleSaveStepData({ step: steps.BANK_DATA, data })}
        />
      </div>
    </section>
  );
}
