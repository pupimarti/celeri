import React from "react";
import Button from "../Button";

export default function Error({ resetSteps }) {
  return (
    <div>
      <p>Ocurrió un error</p>
      <Button text="Volver a empezar" onClick={resetSteps} />
    </div>
  );
}
