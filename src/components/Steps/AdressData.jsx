import React, { useEffect, useState } from "react";
import { handleSaveStepData, Icons, steps } from "src/utils";
import Button from "../Button";
import Input from "../Input";

const initialData = {
  country: "",
  province: "",
  city: "",
  number: "",
  street: "",
  apartment: "",
  postalCode: "",
};

export default function AdressData({ prevStep }) {
  const [data, setData] = useState(
    JSON.parse(localStorage.getItem(`data-step${steps.ADDRESS_DATA}`)) ||
      initialData
  );

  useEffect(() => {
    const [apiAddress] = JSON.parse(localStorage.getItem("domicilio")) ?? [
      null,
    ];
    if (apiAddress) {
      setData({
        country: "",
        province: apiAddress.descripcionProvincia || "",
        city: apiAddress.localidad || "",
        number: apiAddress.numero || "",
        street: apiAddress.direccion || "",
        apartment: apiAddress.piso || "",
        postalCode: apiAddress.codigoPostal || "",
      });
    }
  }, []);

  const handleChangeData = ({ value, name }) =>
    setData((prevData) => ({ ...prevData, [name]: value }));

  return (
    <section className="form-section">
      <div className="container-title-form">
        <Icons.HOME
          color="var(--primary)"
          style={{ marginRight: "10px" }}
          size={20}
        />
        <h2>Dirección</h2>
      </div>
      <Input
        label="País"
        value={data.country}
        name="country"
        type="text"
        required
        onChange={handleChangeData}
      />

      <Input
        label="Provincia"
        value={data.province}
        name="province"
        type="text"
        required
        onChange={handleChangeData}
      />
      <Input
        label="Ciudad o Localidad"
        value={data.city}
        name="city"
        type="text"
        required
        onChange={handleChangeData}
      />

      <Input
        label="Calle"
        value={data.street}
        name="street"
        type="text"
        required
        onChange={handleChangeData}
      />

      <Input
        label="Altura"
        value={data.number}
        name="number"
        type="number"
        required
        onChange={handleChangeData}
      />
      <Input
        label="Piso y/o Depto"
        value={data.apartment}
        name="apartment"
        type="text"
        onChange={handleChangeData}
      />

      <Input
        label="Código postal"
        value={data.postalCode}
        name="postalCode"
        type="text"
        required
        onChange={handleChangeData}
      />
      <div className="container-buttons">
        <Button text="Anterior paso" type="secondary" onClick={prevStep} />
        <Button
          text="Próximo paso"
          submit
          onClick={() => handleSaveStepData({ step: steps.ADDRESS_DATA, data })}
        />
      </div>
    </section>
  );
}
