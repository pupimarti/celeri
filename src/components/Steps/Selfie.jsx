import React, { useState } from "react";
import { handleSaveStepData, steps } from "src/utils";
import Button from "../Button";
import CaptureImage from "../CaptureImage";

const initialData = {
  img: null,
};

export default function Selfie() {
  const [data, setData] = useState(
    JSON.parse(localStorage.getItem(`data-step${steps.SELFIE}`)) || initialData
  );
  const [takePhoto, setTakePhoto] = useState(false);

  const handleTakePhoto = (image) => {
    setTakePhoto(false);
    setData({ img: image });
  };

  if (takePhoto)
    return (
      <CaptureImage
        videoConstraints={{
          facingMode: "user",
        }}
        withoutDniBox
        onScreenShot={handleTakePhoto}
      />
    );

  if (data.img)
    return (
      <div>
        <img src={data.img} alt="Imagen frente del DNI" className="dni-image" />
        <div className="container-buttons">
          <Button
            text="Tomar otra foto"
            type="secondary"
            onClick={() => setData({ img: null })}
          />

          <Button
            text="Confirmar"
            submit
            onClick={() => handleSaveStepData({ step: steps.SELFIE, data })}
          />
        </div>
      </div>
    );

  return (
    <section>
      <p>¡Excelente!</p>
      <p>Por último, necesitamos que te saques una selfie con DNI en mano.</p>
      <p className="text-important">
        Importante: sostené el DNI en frente tuyo
      </p>
      <div style={{ flexDirection: "column" }} className="container-buttons">
        <Button text="Sacar foto" onClick={() => setTakePhoto(true)} />
      </div>
    </section>
  );
}
