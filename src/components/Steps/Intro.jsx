import React from "react";
import Button from "../Button";

export default function Intro({ nextStep }) {
  return (
    <div>
      <p>Abrí tu cuenta en minutos</p>
      <Button text="Comenzar" onClick={() => nextStep()} />
    </div>
  );
}
