import React, { useState } from "react";
import { handleSaveStepData, Icons, steps } from "src/utils";
import Button from "../Button";
import Checkbox from "../Checkbox";
import Input from "../Input";

const initialData = {
  phone: "",
  email: "",
  typeDni: "",
  dni: "",
};

export default function InitialData({ prevStep }) {
  const [data, setData] = useState(
    JSON.parse(localStorage.getItem(`data-step${steps.INITIAL_DATA}`)) ||
      initialData
  );
  const handleChangeData = ({ value, name }) =>
    setData((prevData) => ({ ...prevData, [name]: value }));
  const [acceptDeclare, setAcceptDeclare] = useState(false);

  return (
    <section className="form-section">
      <Input
        Icon={Icons.PHONE}
        placeholder="Teléfono"
        value={data.phone}
        name="phone"
        type="number"
        required
        onChange={handleChangeData}
      />
      <Input
        Icon={Icons.EMAIL}
        placeholder="Email"
        value={data.email}
        name="email"
        type="email"
        required
        onChange={handleChangeData}
      />
      <Input
        Icon={Icons.DNI}
        placeholder="Tipo de Documento"
        value={data.typeDni}
        name="typeDni"
        select
        required
        options={["DNI", "Pasaporte", "LC", "LE"]}
        onChange={handleChangeData}
      />
      <Input
        Icon={Icons.DNI}
        placeholder="Numero de Documento"
        value={data.dni}
        name="dni"
        required
        type="number"
        onChange={handleChangeData}
      />
      <span
        className={`description ${acceptDeclare ? "description-accepted" : ""}`}
      >
        <Checkbox
          checked={acceptDeclare}
          onClick={() => setAcceptDeclare(!acceptDeclare)}
        />
        Declaro bajo juramento que toda la información consignada en el presente
        formulario es dehaciente y he leído y acepto lso términos de la Apertura
        de Cuenta Corriente
      </span>
      <div className="container-buttons">
        <Button text="Anterior paso" type="secondary" onClick={prevStep} />
        <Button
          text="Próximo paso"
          submit
          onClick={() => handleSaveStepData({ step: steps.INITIAL_DATA, data })}
          disabled={!acceptDeclare}
        />
      </div>
    </section>
  );
}
