import React, { useState } from "react";
import { handleSaveStepData, steps } from "src/utils";
import Button from "../Button";
import Checkbox from "../Checkbox";

const initialData = {
  conditions: [
    {
      name: "SOI",
      value: false,
      id: 0,
    },
    {
      name: "PEP",
      value: false,
      id: 1,
    },
    {
      name: "FATCA",
      value: false,
      id: 2,
    },
    {
      name: "Ninguna",
      value: false,
      id: 3,
    },
  ],
  taxesInOtherCountry: false,
  foundsIlicitSources: true,
};

export default function LegalData({ prevStep }) {
  const [data, setData] = useState(
    JSON.parse(localStorage.getItem(`data-step${steps.LEGAL_DATA}`)) ||
      initialData
  );
  const handleChangeData = ({ value, name }) =>
    setData((prevData) => ({ ...prevData, [name]: value }));

  const handleSetConditions = ({ value, id }) =>
    setData((prevData) => ({
      ...prevData,
      conditions: prevData.conditions.map((condition) =>
        condition.id === id ? { ...condition, value } : condition
      ),
    }));

  return (
    <section className="form-section">
      <fieldset>
        <legend className="text-info">
          ¿Cumplís con alguna de las siguientes condiciones?
        </legend>
        {data.conditions.map(({ value, name, id }) => (
          <div key={id} className="container-check-option">
            <Checkbox
              checked={value}
              onClick={() => handleSetConditions({ value: !value, id })}
            />
            {name}
          </div>
        ))}
      </fieldset>
      <fieldset>
        <legend className="text-info">
          ¿Tributas en otro país además de Argentina?
        </legend>
        <div className="container-check-option">
          <Checkbox
            radius
            checked={data.taxesInOtherCountry}
            onClick={() =>
              handleChangeData({
                value: true,
                name: "taxesInOtherCountry",
              })
            }
          />
          SI
        </div>

        <div className="container-check-option">
          <Checkbox
            radius
            checked={!data.taxesInOtherCountry}
            onClick={() =>
              handleChangeData({
                value: false,
                name: "taxesInOtherCountry",
              })
            }
          />
          NO
        </div>
      </fieldset>

      <fieldset>
        <legend className="text-info">
          ¿Sus fondos provienen de orígenes lícitos?
        </legend>
        <div className="container-check-option">
          <Checkbox
            radius
            checked={data.foundsIlicitSources}
            onClick={() =>
              handleChangeData({
                value: true,
                name: "foundsIlicitSources",
              })
            }
          />
          SI
        </div>
        <div className="container-check-option">
          <Checkbox
            radius
            checked={!data.foundsIlicitSources}
            onClick={() =>
              handleChangeData({
                value: false,
                name: "foundsIlicitSources",
              })
            }
          />
          NO
        </div>
      </fieldset>

      <div className="container-buttons">
        <Button text="Anterior paso" type="secondary" onClick={prevStep} />
        <Button
          text="Próximo paso"
          submit
          onClick={() => handleSaveStepData({ step: steps.LEGAL_DATA, data })}
        />
      </div>
    </section>
  );
}
