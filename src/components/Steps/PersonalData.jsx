import React, { useEffect, useState } from "react";
import getCuit from "src/services/getCuit";
import getPersonData from "src/services/getPersonData";
import { handleSaveStepData, Icons, steps } from "src/utils";
import Button from "../Button";
import Input from "../Input";
import Spinner from "../Spinner";

const initialData = {
  firstName: "",
  secondName: "",
  lastname: "",
  cuil: "",
  genre: "",
  nationality: "",
  country: "",
  dateOfBirth: "",
};

export default function PersonalData({ prevStep }) {
  const [data, setData] = useState(
    JSON.parse(localStorage.getItem(`data-step${steps.PERSONAL_DATA}`)) ||
      initialData
  );

  const [loading, setLoading] = useState(true);

  const [error, setError] = useState("");

  useEffect(() => {
    const getPersonalData = async (dni) => {
      try {
        const { idPersona } = await getCuit({ dni });
        const { persona } = await getPersonData({ cuit: idPersona });
        if (persona) {
          localStorage.setItem("domicilio", JSON.stringify(persona.domicilio));
          setData({
            firstName: persona.nombre,
            secondName: "",
            lastname: persona.apellido,
            cuil: persona.idPersona,
            genre: "",
            nationality: "",
            country: "",
            dateOfBirth: new Date(persona.fechaNacimiento).toLocaleDateString(
              "en-US"
            ),
          });
          setLoading(false);
        }
      } catch (e) {
        setLoading(false);
        setError(
          "Ocurrio un error al buscar sus datos, por favor ingreselos manualmente."
        );
      }
    };

    const userData = JSON.parse(
      localStorage.getItem(`data-step${steps.INITIAL_DATA}`)
    );
    if (userData?.dni) {
      getPersonalData(userData.dni);
    } else setLoading(false);
  }, []);

  const handleChangeData = ({ value, name }) =>
    setData((prevData) => ({ ...prevData, [name]: value }));

  if (loading)
    return (
      <div
        style={{
          display: "flex",
          justifyContent: "center",
          alignItems: "center",
        }}
      >
        <Spinner />
      </div>
    );

  return (
    <section className="form-section">
      <div className="container-title-form">
        <Icons.PERSON
          color="var(--primary)"
          style={{ marginRight: "10px" }}
          size={20}
        />
        <h2>Datos Personales</h2>
      </div>
      {error && (
        <p style={{ marginBottom: "20px" }} className="text-important">
          {error}
        </p>
      )}
      <Input
        placeholder="Primer Nombre"
        label="Primer Nombre"
        value={data.firstName}
        name="firstName"
        required
        type="text"
        onChange={handleChangeData}
      />
      <Input
        placeholder="Segundo Nombre"
        label="Segundo Nombre"
        value={data.secondName}
        name="secondName"
        type="text"
        onChange={handleChangeData}
      />
      <Input
        placeholder="Apellidos"
        label="Apellidos"
        value={data.lastname}
        required
        name="lastname"
        type="text"
        onChange={handleChangeData}
      />
      <Input
        placeholder="Cuil/Cuit"
        label="Cuil/Cuit"
        value={data.cuil}
        required
        name="cuil"
        type="number"
        onChange={handleChangeData}
      />
      <Input
        placeholder="Género"
        label="Género"
        value={data.genre}
        required
        name="genre"
        select
        options={["Masculino", "Femenino"]}
        onChange={handleChangeData}
      />

      <Input
        placeholder="Nacionalidad"
        label="Nacionalidad"
        value={data.nationality}
        required
        name="nationality"
        type="text"
        onChange={handleChangeData}
      />

      <Input
        placeholder="País de nacimiento"
        label="País de nacimiento"
        value={data.country}
        required
        name="country"
        type="text"
        onChange={handleChangeData}
      />

      <Input
        placeholder="Fecha Nacimiento"
        label="Fecha Nacimiento"
        value={data.dateOfBirth}
        required
        name="dateOfBirth"
        type="text"
        onChange={handleChangeData}
      />
      <div className="container-buttons">
        <Button text="Anterior paso" type="secondary" onClick={prevStep} />
        <Button
          text="Próximo paso"
          submit
          onClick={() =>
            handleSaveStepData({ step: steps.PERSONAL_DATA, data })
          }
        />
      </div>
    </section>
  );
}
