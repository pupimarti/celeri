import React from "react";

import "./styles.css";

export default function Spinner({ size = 36 }) {
  return (
    <div
      style={{ width: `${size}px`, height: `${size}px` }}
      className="spinner"
    />
  );
}
