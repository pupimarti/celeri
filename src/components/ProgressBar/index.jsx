import React from "react";

import "./styles.css";

export default function ProgressBar({ steps = 8, step = 2 }) {
  return (
    <div className="container-progress-bar">
      <div
        style={{ width: `${(step + 1) * (100 / steps)}%` }}
        className="progress-bar"
      />
      {new Array(steps).fill(0).map((_, index) => (
        <div
          key={index}
          style={{
            left: `${((index + 1) * 100) / steps}%`,
            backgroundColor: index <= step ? "var(--primary)" : "var(--border)",
          }}
          className="progress-bar-step"
        />
      ))}
    </div>
  );
}
