import {
  BsFillPhoneFill,
  BsFillEnvelopeFill,
  BsCoin,
  BsCashCoin,
  BsCreditCard2Front,
  BsFillPersonFill,
  BsFillHouseDoorFill,
} from "react-icons/bs";

export const Icons = {
  PHONE: BsFillPhoneFill,
  EMAIL: BsFillEnvelopeFill,
  DNI: BsCreditCard2Front,
  CBU: BsCashCoin,
  MONEY: BsCoin,
  ARROW_RIGHT: BsFillEnvelopeFill,
  PERSON: BsFillPersonFill,
  HOME: BsFillHouseDoorFill,
};

export const steps = {
  INTRO: 0,
  INITIAL_DATA: 1,
  PERSONAL_DATA: 2,
  ADDRESS_DATA: 3,
  LEGAL_DATA: 4,
  BANK_DATA: 5,
  SAVE_DATA: 6,
  INTRO_PHOTOS: 7,
  DNI_FRONT: 8,
  DNI_BACK: 9,
  SELFIE: 10,
  END: 11,
};

export const clearStorageData = () => {
  localStorage.removeItem("step");
  localStorage.removeItem("domicilio");
  Object.keys(steps).forEach((_, index) =>
    localStorage.removeItem(`data-step${index}`)
  );
};

export const getBase64 = (file) =>
  new Promise((resolve, reject) => {
    const reader = new FileReader();
    reader.onload = () => resolve(reader.result);
    reader.onerror = (error) => reject(error);
    reader.readAsDataURL(file);
  });

export const handleSaveStepData = ({ step, data }) => {
  localStorage.setItem(`data-step${step}`, JSON.stringify(data));
};
