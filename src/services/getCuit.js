import API_URL from "./config";

const GET_CUIT_URL = "ws_sr_padron_a13/getIdPersonaListByDocumento";

const getCuit = ({ dni = "" }) =>
  fetch(`${API_URL}/${GET_CUIT_URL}?documento=${dni}`, {
    method: "GET",
    headers: new Headers({
      Authorization: `Apikey ${process.env.REACT_APP_API_KEY}`,
    }),
  })
    .then((res) => res.json())
    .then((res) => res)
    .catch((error) => {
      throw new Error(error);
    });

export default getCuit;
