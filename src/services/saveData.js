const saveData = () =>
  new Promise((resolve) => {
    const wait = setTimeout(() => {
      clearTimeout(wait);
      resolve(true);
    }, 1500);
  });

export default saveData;
