import API_URL from "./config";

const GET_DATA_FROM_CUIT = "ws_sr_padron_a13/getPersona";

const getPersonData = ({ cuit = "" }) =>
  fetch(`${API_URL}/${GET_DATA_FROM_CUIT}?idPersona=${cuit}`, {
    method: "GET",
    headers: new Headers({
      Authorization: `Apikey ${process.env.REACT_APP_API_KEY}`,
    }),
  })
    .then((res) => res.json())
    .then((res) => res)
    .catch((error) => {
      throw new Error(error);
    });

export default getPersonData;
