import React, { useEffect, useState } from "react";
import logo from "src/assets/logo.png";
import "./App.css";
import Button from "./components/Button";
import ProgressBar from "./components/ProgressBar";
import Steps from "./components/Steps";
import { clearStorageData, steps } from "./utils";

const setpsLength = Object.keys(steps).length;

function App() {
  const [step, setStep] = useState(0);
  const [showModalSelect, setShowModalSelect] = useState(false);

  useEffect(() => {
    const localStep = parseInt(localStorage.getItem("step"), 10);
    if (localStep) setShowModalSelect(true);
  }, []);

  const handleContinueLocalAperture = () => {
    setShowModalSelect(false);
    setStep(parseInt(localStorage.getItem("step"), 10));
  };

  const handleNewAperture = () => {
    setShowModalSelect(false);
    clearStorageData();
  };

  const handleNextStep = () => {
    setStep((prevStep) => {
      if (prevStep === steps.END) {
        clearStorageData();
        return 0;
      }
      return prevStep + 1;
    });
  };

  const handlePrevStep = () => {
    setStep((prevStep) => prevStep - 1);
  };

  const handleResetSteps = () => {
    setStep(0);
  };

  return (
    <div
      style={{ height: `${window.innerHeight}px` }}
      className="container-app"
    >
      <header className="App-header">
        <img src={logo} className="logo" alt="logo" />
      </header>
      {showModalSelect ? (
        <div style={{ display: "flex", flexDirection: "column" }}>
          <p>Tenes una apertura en proceso</p>
          <Button text="Continuarla" onClick={handleContinueLocalAperture} />
          <Button text="Empezar una nueva" onClick={handleNewAperture} />
        </div>
      ) : (
        <>
          <ProgressBar steps={setpsLength} step={step} />
          <Steps
            step={step}
            resetSteps={handleResetSteps}
            nextStep={handleNextStep}
            prevStep={handlePrevStep}
          />
        </>
      )}
    </div>
  );
}

export default App;
