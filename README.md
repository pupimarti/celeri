### Celeri Challenge

The project was created with create-react-app

## Running the project locally

Follow these steps to `start the project` in development

1. Clone repository. `git clone https://gitlab.com/pupimarti/celeri.git`
2. Install dependencies in the project folder running `yarn`
3. Create a `.env` file in the root directory and add in

```
REACT_APP_API_KEY = API_KEY
```

4. Run in local with `yarn start`, this command open the server in `http://localhost:3000`.
